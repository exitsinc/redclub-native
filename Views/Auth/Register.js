import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  TouchableHighlight,
  Picker
} from 'react-native';
import StepIndicator from 'react-native-step-indicator';

export default function Register({navigation}) {
  const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 35,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#E24816',
    stepStrokeWidth: 1,
    stepStrokeFinishedColor: '#E24816',
    stepStrokeUnFinishedColor: '#e24916',
    separatorFinishedColor: '#e24916',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#E24816',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#e24916',
    stepIndicatorLabelFontSize: 15,
    currentStepIndicatorLabelFontSize: 25,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#fe7013',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#fe7013',
  };

  
  const [Gender, setGender] = React.useState('Male');

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View>
          <Text
            style={{
              color: '#E24816',
              fontWeight: 'bold',
              fontSize: 30,
              alignSelf: 'center',
              paddingTop: 180,
              paddingBottom:15
            }}>
            Registration
          </Text>
          <StepIndicator
            customStyles={customStyles}
            currentPosition="1"
            stepCount="3"
          />
          <TextInput
            placeholder="First Name"
            style={{
              margin: 20,
              marginHorizontal: 45,
              borderBottomColor: '#e8e8e8',
              borderBottomWidth: 2,
            }}
          />
          <TextInput
            placeholder="Last Name"
            style={{
              margin: 5,
              marginHorizontal: 45,
              borderBottomColor: '#e8e8e8',
              borderBottomWidth: 2,
            }}
          />
         {/* <TextInput
            placeholder="Gender"
            style={{
              margin: 20,
              marginHorizontal: 45,
              borderBottomColor: '#e8e8e8',
              borderBottomWidth: 2,
            }}
          /> */}
          <View style={{borderBottomColor:"#e8e8e8",borderBottomWidth:2,marginHorizontal:40}}>
          <Picker
                 selectedValue={Gender}
                // style={{ height: 50, width: 150 }}
                style={{
                  // margin: 20,
                  // marginHorizontal: 45,
                  borderBottomColor: '#e8e8e8',
                  borderBottomWidth: 2, 
                  marginTop:15,
                  color:"grey"
                }}
                onValueChange={(itemValue, itemIndex) => {
                  setGender(itemValue);
                  console.log(itemValue, itemIndex);
                }}>
                <Picker.Item label="Male" value="m" />
                <Picker.Item label="Female" value="f" />
                <Picker.Item label="Other" value="o" />
              </Picker>
              </View>
          <TouchableOpacity
            style={{
              margin: 15,
              marginHorizontal: 45,
              backgroundColor: '#e24916',
              padding: 10,
              borderRadius: 20,
            }}
            onPress={() => navigation.navigate('Register2')}>
            <Text style={{alignSelf: 'center', color: 'white'}}>Next</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text
              style={{
                color: '#1E8FE7',
                alignSelf: 'flex-end',
                marginHorizontal: 50,
                fontWeight: 'bold',
              }}>
              Already on Red Club?
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusbar: {
    width: '80%',
    height: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    borderWidth: 1,
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#E24816',
    color: '#E24816',
  },
  text1: {
    borderWidth: 1,
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#E24816',
    borderColor: '#E24816',
    color: 'white',
  },
  input: {
    width: '80%',
    height: 30,
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  btn: {
    backgroundColor: '#E24816',
    width: '80%',
    height: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
});
