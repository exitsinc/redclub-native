import React from 'react'
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from '../../../Views/Auth/SplashScreen';
import Login from '../../../Views/Auth/Login';
import Register from '../../../Views/Auth/Register';
import Register2 from '../../../Views/Auth/Register2';
import Register3 from '../../../Views/Auth/Register3';
import ListYourProfile from '../../../Views/Auth/ListYourProfile';
import ListYourProfile2 from '../../../Views/Auth/ListYourProfile2';
import ListYourProfile3 from '../../../Views/Auth/ListYourProfile3';


const Stack = createStackNavigator();
export default function AuthStacks() {
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Register2" component={Register2} />
        <Stack.Screen name="Register3" component={Register3} />
        <Stack.Screen name="ListYourProfile" component={ListYourProfile} />
        <Stack.Screen name="ListYourProfile2" component={ListYourProfile2} />
        <Stack.Screen name="ListYourProfile3" component={ListYourProfile3} />
      </Stack.Navigator>
    )
}
