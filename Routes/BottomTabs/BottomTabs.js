import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeStack from '../Stacks/HomeStacks/HomeStack';
import Icon from 'react-native-vector-icons/AntDesign';
import Profile from '../../Views/ProfileScreen/Profile';
import ProfileStacks from '../Stacks/ProfileStacks/ProfileStacks';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
const Tab = createBottomTabNavigator();
export default function BottomTabs() {
    return (
        <Tab.Navigator initialRouteName='HomeStack' tabBarOptions={{
            keyboardHidesTabBar: true,
            activeTintColor: 'white',
            // inactiveTintColor:'red',
            activeBackgroundColor:'#E24816',
            
            showLabel: false,
            style: {
                backgroundColor: 'white',
                position: 'absolute',
                // bottom: 10,
                // marginHorizontal: 20,
                // Max Height...
                // height: 60,
                // borderRadius: 10,
                // Shadow...
                shadowColor: '#000',
                shadowOpacity: 0.06,
                shadowOffset: {
                    width: 10,
                    height: 10
                },
                // paddingHorizontal: 20,
            }
        }}>
            <Tab.Screen name="Search" component={HomeStack}
            
                options={{    
                    tabBarIcon: ({ color, size }) => (
                        <>
                        <FontAwesome5Icon name="search" color={color} size={20} />
                        {/* <Text>Search</Text> */}
                        </>
                    ),
                    // tabBarBadge: 3,

                }} />
            <Tab.Screen name="Favourite" component={ProfileStacks}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <>
                        <FontAwesome5Icon name="heart" color={color} size={20} />
                        {/* <Text>Favourite</Text> */}
                        </>
                    ),
                    // tabBarBadge: 5,
                }} />
            
            <Tab.Screen name="Chat" component={HomeStack}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <>
                        <FontAwesome5Icon name="comments" color={color} size={20} />
                        {/* <Text style={{color:{color}}}>Chat</Text> */}
                        </>
                    ),
                }} />
        </Tab.Navigator>

    )
}
