import React from "react";
import { View, StyleSheet } from "react-native";

import SwitchSelector from "react-native-switch-selector";

const options = [
  { label: "Meet Up", value: "1" },
  { label: "Virtual Dating", value: "1.5" }
];

const SwitchSelec = () => {
  return (
    <View style={styles.switchsel}>
      <SwitchSelector
        options={options}
        initial={0}
        onPress={(value) => console.log(`Call onPress with value: ${value}`)}
        textColor={"black"} //'#7a44cf'
        selectedColor={"white"}
        buttonColor={"#E24816"}
        borderColor={"#E24816"}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    padding: 10,
    flex: 1,
    backgroundColor: "#F8F8FF",
    alignItems: "center"
  },
  switchsel: {
    width: "90%",
    // justifyContent: "center"
    // alignItems:'center',
  }
});

export default SwitchSelec;
