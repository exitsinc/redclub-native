import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import StarRating from 'react-native-star-rating-widget';
import {SliderBox} from 'react-native-image-slider-box';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const ProfileCard = ({navigation}) => {
  const [rating, setRating] = React.useState(0);

  const images = [
    require('../../../assets/girl1.jpg'),
    require('../../../assets/girl3.jpg'),
    require('../../../assets/girl2.jpg'),
    'https://source.unsplash.com/1024x768/?girl',
    require('../../../assets/girl4.jpg'), // Network image
    require('../../../assets/girl5.jpg'), // Local image
  ];

  return (
    <SafeAreaView>
      <ScrollView style={{marginBottom: 50}}>
        <View>
          <View style={{}}>
            {/* <ScrollView
          pagingEnabled
          horizontal
          style={{windowWidth, windowHeight, alignSelf: 'center', padding: 10}}>
            <View style={{flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
        <Image
          source={require('../../../assets/dp.jpg')}
          style={{
            height: windowHeight / 2,
            width: windowWidth,
            borderRadius: 10,
            padding:10
          }}
          resizeMode="contain"
        />
        <Image
          source={require('../../../assets/logo.png')}
          style={{
            height: windowHeight / 2,
            width: windowWidth,
            borderRadius: 10,
            padding:10
          }}
          resizeMode="contain"
        />
        <Image
          source={require('../../../assets/dp.jpg')}
          style={{
            height: windowHeight / 2,
            width: windowWidth,
            borderRadius: 10,
            padding:10
          }}
          resizeMode="contain"
        />
</View>
        </ScrollView> */}
            <SliderBox
              images={images}
              sliderBoxHeight={400}
              dotColor="#E24816"
              // circleLoop
            />
            <View style={{position:"absolute",padding:10,marginLeft:"65%",flexDirection:"column",alignItems:"flex-end",justifyContent:"flex-end"}}>
           <TouchableOpacity>
            <FontAwesome5Icon
                  name="fire"
                  color="#E24816"
                  size={20}
                  style={{}}
                />
                
                </TouchableOpacity>
                <Text style={{marginRight:-10}}>18.5K</Text>
                
                </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              paddingTop: 20,
              paddingHorizontal: 10,
              justifyContent: 'space-between',
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{fontSize: 25, marginTop: 10}}>Alisha Bhatt</Text>
              {/* <Text
            style={{
              margin: 10,
              marginLeft: 10,
              backgroundColor: 'gold',
              borderRadius: 10,
              paddingHorizontal: 10,
            }}>
            VIP
          </Text> */}
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                style={{
                  borderTopEndRadius: 15,
                  borderBottomEndRadius: 15,
                  borderBottomStartRadius: 15,
                  borderTopStartRadius: 15,
                  // backgroundColor: '#1E8EF7',
                  // paddingHorizontal: 10,
                  // padding: 10,
                  // margin: 10,
                  flexDirection: 'row',
                }}>
                <FontAwesome5Icon name="phone-alt" color="#E24816" size={20} />
                {/* <Text style={{paddingLeft:5,color:"white"}}>Call</Text> */}
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  borderTopEndRadius: 15,
                  borderBottomEndRadius: 15,
                  borderBottomStartRadius: 15,
                  borderTopStartRadius: 15,
                  // backgroundColor: '#1E8EF7',
                  paddingHorizontal: 10,
                  // padding: 10,
                  margin: 10,
                  flexDirection: 'row',
                }}>
                <FontAwesome5Icon
                  name="comment-dots"
                  color="#E24816"
                  size={20}
                />
                {/* <Text style={{paddingLeft:5,color:"white"}}>Chat</Text> */}
              </TouchableOpacity>

              <TouchableOpacity
                style={{justifyContent: 'center', alignItems: 'center'}}>
                <FontAwesomeIcon name="heart" color="red" size={20} />
                {/* <Text style={{alignSelf: 'flex-start'}}>74.5K</Text> */}
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 10,
            }}>
            <FontAwesome5Icon name="female" size={15} color="#E24816" />
            <Text style={{marginLeft: 5}}>21</Text>
            <FontAwesome5Icon
              name="map-marker-alt"
              color="#E24816"
              size={15}
              style={{marginLeft: 20}}
            />
            <Text style={{marginLeft: 5}}>Uttarakhand</Text>

            {/* <Text
          style={{
            marginLeft: 10,
            backgroundColor: 'grey',
            color: 'white',
            paddingHorizontal: 2,
          }}>
          ID
        </Text>
        <Text style={{marginLeft: 5}}>72361232</Text> */}
          </View>
          <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 15,
            }}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                backgroundColor: '#E24816',
                borderRadius: 10,
                paddingHorizontal: 5,
              }}>
              {/* <FontAwesome5Icon name="sliders-h" color="white" size={15} /> */}
              <Text style={{marginHorizontal: 5, color: 'white'}}>
                About Me
              </Text>
            </TouchableOpacity>
          </View>
          <View>
            <Text
              style={{
                marginLeft: 10,
                color: 'grey',
                paddingHorizontal: 2,
                paddingTop: 10,
                borderRadius: 10,
              }}>
              I'm from Amsterdam.. Stuck in Mumbai due to Covid! Looking for a
              fun person! Into Casual Dating
            </Text>
          </View>
          <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 15,
            }}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                backgroundColor: '#E24816',
                borderRadius: 10,
                paddingHorizontal: 5,
              }}>
              {/* <FontAwesome5Icon name="sliders-h" color="white" size={15} /> */}
              <Text style={{marginHorizontal: 5, color: 'white'}}>
                My Interests
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 10,
            }}>
            <Text
              style={{
                marginLeft: 0,
                color: 'grey',
                paddingHorizontal: 2,
                borderRadius: 10,
              }}>
              Design
            </Text>
            <Text style={{marginLeft: 10, color: 'grey'}}>Signing</Text>
            <Text style={{marginLeft: 10, color: 'grey'}}>Video Games</Text>
          </View>
          <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 15,
            }}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                backgroundColor: '#E24816',
                borderRadius: 10,
                paddingHorizontal: 5,
              }}>
              {/* <FontAwesome5Icon name="sliders-h" color="white" size={15} /> */}
              <Text style={{marginHorizontal: 5, color: 'white'}}>
                My Work / My Education
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 10,
            }}>
            <Text
              style={{
                marginLeft: 0,
                color: 'grey',
                paddingHorizontal: 2,
                borderRadius: 10,
              }}>
              Graduate
            </Text>
            <Text style={{marginLeft: 10, color: 'grey'}}>Option 1</Text>
            <Text style={{marginLeft: 10, color: 'grey'}}>Option 2</Text>
          </View>

          <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 15,
            }}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                backgroundColor: '#E24816',
                borderRadius: 10,
                paddingHorizontal: 5,
              }}>
              {/* <FontAwesome5Icon name="sliders-h" color="white" size={15} /> */}
              <Text style={{marginHorizontal: 5, color: 'white'}}>
                Basic Info
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 10,
            }}>
            <Text
              style={{
                marginLeft: 0,
                color: 'grey',
                paddingHorizontal: 2,
                borderRadius: 10,
              }}>
              Graduate
            </Text>
            <Text style={{marginLeft: 10, color: 'grey'}}>Option 1</Text>
            <Text style={{marginLeft: 10, color: 'grey'}}>Option 2</Text>
          </View>

          {/* <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 15,
            }}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
              }}>
              <Text style={{fontSize: 20, color: 'grey'}}>Comments</Text>
            </TouchableOpacity>

            <StarRating rating={rating} onChange={setRating} />
          </View> */}
          <View
            style={{
              paddingHorizontal: 10,
              flexDirection: 'row',
              paddingTop: 10,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {/* <TouchableOpacity
                        style={{
                          borderTopEndRadius: 15,
                          borderBottomEndRadius: 15,
                          borderBottomStartRadius: 15,
                          borderTopStartRadius: 15,
                          backgroundColor: '#1E8EF7',
                          paddingHorizontal: 20,
                          padding: 10,
                          margin: 10,
                          flexDirection:"row"
                        }}>
                        <FontAwesome5Icon
                          name="phone-alt"
                          color="white"
                          size={15}
                        />
                        <Text style={{paddingLeft:5,color:"white"}}>Call</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={{
                          borderTopEndRadius: 15,
                          borderBottomEndRadius: 15,
                          borderBottomStartRadius: 15,
                          borderTopStartRadius: 15,
                          backgroundColor: '#1E8EF7',
                          paddingHorizontal: 20,
                          padding: 10,
                          margin: 10,
                          flexDirection:"row"
                        }}>
                        <FontAwesome5Icon
                          name="comment-dots"
                          color="white"
                          size={18}
                        />
                        <Text style={{paddingLeft:5,color:"white"}}>Chat</Text>
                      </TouchableOpacity> */}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ProfileCard;

const styles = StyleSheet.create({});
