import React from 'react'
import {
  Image,
  View,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  Text,
  StyleSheet,
  ScrollView
} from 'react-native';
import ProfileCard from '../../Components/AppComponents/HomeComponents/ProfileCard';
import Header from '../../Components/Header';
import styles from '../../Components/Styles/Styles';

function Profile({navigation,route}) {

  return (
    <SafeAreaView style={styles.container}>
     <Header title="Home" navigation={navigation} noFab={true} route={route}>
    <View style={{flex:1}}>
      <ScrollView>
      <ProfileCard/>
      </ScrollView>
    </View>
    </Header>
  </SafeAreaView>
  );
}
export default Profile;
