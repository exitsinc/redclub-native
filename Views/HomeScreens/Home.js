import React from 'react'
import Header from '../../Components/Header';

import Icon from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons'
import EvilIcons from 'react-native-vector-icons/EvilIcons'


import Entypo from 'react-native-vector-icons/Entypo'
import { View, Image, Text, SafeAreaView, StyleSheet, ScrollView, True, ScrollViewBase } from 'react-native'
import { Avatar, Title, Caption, TouchableRipple } from 'react-native-paper'
import SwitchSelec from '../../Components/AppComponents/HomeComponents/SwitchSelec';

import Card from '../../Components/AppComponents/HomeComponents/Card';

function Home({ navigation, route }) {
  return (
    <SafeAreaView style={styles.container}>
     <Header title="Home" navigation={navigation} noFab={true} route={route}>
    <View style={{flex:1,margin:10,marginBottom:45}}>
      <View style={{alignItems:"center"}}>
 
      <SwitchSelec/>
      </View> 
      <ScrollView>
      <Card/>
      </ScrollView>
      {/* <Card/>
      <Card/> */}
    </View>
    </Header>
  </SafeAreaView>
  );
}
export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
})