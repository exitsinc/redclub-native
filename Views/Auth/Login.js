import React from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
import {Button} from 'react-native-paper';
import { useDispatch } from 'react-redux';
import InputForm from '../../Components/Form/InputForm';
import { setIsLogged } from '../../Redux/Slice/LoginSlice';


export default function Login({navigation}) {

  const dispatch = useDispatch()
  const submitHandler=(values)=>{
    console.log(values)
     dispatch(setIsLogged(true));
     navigation.replace('Home')
  }
  return (
    <SafeAreaView >
     <View style={{backgroundColor:"white",height:"100%"}}>
       <View style={{alignItems:"center",marginTop:"40%"}}>
       <Image
          style={styles.img}
          source={require('../../assets/logo.png')}
        />
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-evenly",marginTop:"40%"}}>
       <TouchableOpacity style={styles.loginbtn} onPress={submitHandler} >
            <Text style={{ color: "#E24816" }}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.signupbtn} onPress={() => navigation.navigate('Register')}>
            <Text style={{ color: "#1E8EF7" }}>Sign Up</Text>
          </TouchableOpacity>
     </View>
     <View style={{alignItems:"center",marginTop:"25%"}}>
     <TouchableOpacity style={{}} onPress={() => navigation.navigate('ListYourProfile')}>
            <Text style={{ color: "grey" }}>List Your Profile?</Text>
          </TouchableOpacity>
          </View>
     </View>
       
  
        
  </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  img:{
    width:200,
    height:200
  },
  loginbtn:{
    borderColor: "#E24816",
    borderWidth: 1,
    borderRadius: 3,
    paddingHorizontal:50,
    paddingVertical:15
  },
  signupbtn:
  {
    borderColor: "#1E8EF7",
    borderWidth: 1,
    borderRadius: 3,
    paddingHorizontal:50,
    paddingVertical:15
  }
  
});
