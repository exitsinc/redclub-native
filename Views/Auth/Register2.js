import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  TouchableHighlight,
} from 'react-native';
import StepIndicator from 'react-native-step-indicator';

export default function Register2({navigation}) {
  const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 35,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#E24816',
    stepStrokeWidth: 1,
    stepStrokeFinishedColor: '#E24816',
    stepStrokeUnFinishedColor: '#e24916',
    separatorFinishedColor: '#e24916',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#E24816',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#e24916',
    stepIndicatorLabelFontSize: 15,
    currentStepIndicatorLabelFontSize: 25,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#fe7013',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#fe7013',
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View>
          <Text
            style={{
              color: '#E24816',
              fontWeight: 'bold',
              fontSize: 30,
              alignSelf: 'center',
              paddingTop: 180,
              paddingBottom:15
            }}>
            Registration
          </Text>
          <StepIndicator
            customStyles={customStyles}
            currentPosition="2"
            stepCount="3"
          />
          <TextInput
            placeholder="Mobile Number"
            style={{
              margin: 20,
              marginHorizontal: 45,
              borderBottomColor: '#e8e8e8',
              borderBottomWidth: 2,
            }}
          />
          <TextInput
            placeholder="Email"
            style={{
              margin: 5,
              marginHorizontal: 45,
              borderBottomColor: '#e8e8e8',
              borderBottomWidth: 2,
            }}
          />
          <TextInput
            placeholder="City"
            style={{
              margin: 20,
              marginHorizontal: 45,
              borderBottomColor: '#e8e8e8',
              borderBottomWidth: 2,
            }}
          />
          <View style={{flexDirection: 'row', justifyContent:"space-between",marginHorizontal:40,margin:10}}>
            <TouchableOpacity style={{ backgroundColor:"#1E8EF7", borderRadius:20,padding:10,width:"45%"}} onPress={() => navigation.goBack()}>
              <Text style={{alignSelf: 'center', color: 'white'}}>
                Previous
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ backgroundColor:"#E24816", borderRadius:20,padding:10,width:"45%"}} onPress={() => navigation.navigate('Register3')}>
              <Text style={{alignSelf: 'center', color: 'white'}}>Next</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity>
            <Text
              style={{
                color: '#1E8FE7',
                alignSelf: 'flex-end',
                marginHorizontal: 50,
                fontWeight: 'bold',
              }}>
              Already on Red Club?
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusbar: {
    width: '80%',
    height: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    borderWidth: 1,
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#E24816',
    color: '#E24816',
  },
  text1: {
    borderWidth: 1,
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#E24816',
    borderColor: '#E24816',
    color: 'white',
  },
  input: {
    width: '80%',
    height: 30,
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  btn: {
    backgroundColor: '#E24816',
    width: '80%',
    height: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
});
