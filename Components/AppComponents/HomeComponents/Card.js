import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {windowHeight, windowWidth} from '../../../Util/Dimensions';
// import HeartIcon from '../../Global/HeartIcon'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import {deleteDataById} from '../../../Axios/axios';
import {useDispatch} from 'react-redux';

export default function Card({navigation}) {
  const dispatch = useDispatch();
  // const {driverData} = props;

  const FootTab = [
    {
      id: 1,
      name: 'Shonam Sharma',
      rating: '4.1',
      model: '21',
      map: '2Km',
      
    },
    {
      id: 2,
      name: 'Shonam Sharma',
      rating: '4.1',
      model: '21',
      map: '2Km',
      
    },
    {
      id: 3,
      name: 'Shonam Sharma',
      rating: '4.1',
      model: '21',
      map: '2Km',
      
    },
    // {
    //   id: 4,
    //   name: 'Shonam Sharma',
    //   rating: '4.1',
    //   model: '21',
    // },
  ];


  return (
    <View>
      <View>
        <FlatList
          // data={driverData}
          data={FootTab}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{paddingTop: 20}}
          renderItem={({item}) => (
            <>
              <TouchableOpacity  >
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  margin: 10,
                  padding: 10,
                  elevation: 3,
                  backgroundColor: 'white',
                  marginHorizontal: 2,
                  borderRadius: 1,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    source={require('../../../assets/dp.jpg')}
                    style={{height: 110, width: 100, borderRadius: 10}}
                  />

                  <View style={{marginLeft: 20}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={{fontSize: 15, fontWeight: 'bold'}}>
                        {/* {item.name.length < 10
                        ? `${item.name} `
                        : `${item.name.substring(0, 10)}...`} */}
                        {item.name}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 10,
                      }}>
                      <View style={{flexDirection: 'row'}}>
                        <FontAwesome5Icon
                          name="star"
                          size={15}
                          color="red"
                          style={{marginTop: 4}}
                        />
                        <Text style={{color: 'grey', margin: 4}}>
                          {item.rating}
                        </Text>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <FontAwesome5Icon
                          name="female"
                          size={15}
                          color="red"
                          style={{marginTop: 4}}
                        />
                        <Text style={{color: 'grey', margin: 4, marginLeft: 5}}>
                          {item.model}
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 5,
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      {/* <TouchableOpacity
                        style={{
                          borderTopEndRadius: 15,
                          borderBottomEndRadius: 15,
                          borderBottomStartRadius: 15,
                          borderTopStartRadius: 15,
                          backgroundColor: '#1E8EF7',
                          paddingHorizontal: 20,
                          padding: 10,
                          flexDirection:"row"
                        }}>
                        <FontAwesome5Icon
                          name="phone-alt"
                          color="white"
                          size={15}
                        />
                        <Text style={{paddingLeft:5,color:"white"}}>Call</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={{
                          borderTopEndRadius: 15,
                          borderBottomEndRadius: 15,
                          borderBottomStartRadius: 15,
                          borderTopStartRadius: 15,
                          backgroundColor: '#1E8EF7',
                          paddingHorizontal: 20,
                          padding: 10,
                          margin: 10,
                          flexDirection:"row"
                        }}>
                        <FontAwesome5Icon
                          name="comment-dots"
                          color="white"
                          size={18}
                        />
                        <Text style={{paddingLeft:5,color:"white"}}>Chat</Text>
                      </TouchableOpacity> */}

                      <View style={{flexDirection: 'row'}}>
                        <FontAwesome5Icon
                          name="map-marker-alt"
                          size={15}
                          color="red"
                          style={{marginTop: 4}}
                        />
                        <Text style={{color: 'grey', margin: 4, marginLeft: 5}}>
                          {item.map}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
                {/* <HeartIcon icon="Ionicons" name="heart-outline" size={25} color="#f06b32" /> */}
                <View style={{}}>
                  <TouchableOpacity>
                    <FontAwesomeIcon name="heart" color="red" size={18} />
                  </TouchableOpacity>
                </View>
                
              </View>
              

              {/* <View style={{borderWidth:0.2,marginTop:10}}/> */}
              </TouchableOpacity>
            </>
          )}
        />
      </View>
    </View>
  );
}
